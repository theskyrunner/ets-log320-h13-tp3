/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-06              Class created;
 *  2013-03-08              Starting Client and communictions;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought;

import java.io.IOException;

import ca.etsmtl.cours.log320.breakTrought.ai.NegaMax;
import ca.etsmtl.cours.log320.breakTrought.evals.EvalSimple;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.moves.AMoveGenerator;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;


/**
 * Entry point of the Application.
 * @author Nicolas
 */
public final class BreakThrough
{
	//============================================================================
	// STATIC ASSERTIONS
	//============================================================================
	public static final String D_HOST  = "localhost";
	public static final String C_HOST  = "app.breakthrough-etsmtl.com";
	public static final int    D_PORT  = 8888;
	public static final int    C_PORT  = 8181;
	
	public static final int ROW_LENGTH = 8;
	public static final int BOARD      = BreakThrough.ROW_LENGTH * BreakThrough.ROW_LENGTH;
	public static final int FIELD_SIZE = BreakThrough.BOARD - BreakThrough.ROW_LENGTH;
	public static final int WHITE      = 4;
	public static final int BLACK      = 2;

	public static final long TIME_MAX       = 4900L;
	public static final long TIME_SLEEP     = 4500L;
	public static final long TIME_INTERVAL  = 102L;
	public static final long TIME_PADDING   = BreakThrough.TIME_INTERVAL * 2;
	
	private static BreakThrough game;
	
	public static final void main(final String[] args)
	{
		// TODO Auto-generated method stub
		String host = (args.length >= 1) ? args[0] : BreakThrough.D_HOST;
		System.out.println(host);
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("Starting AI");
		BreakThrough.game = new BreakThrough();
		BreakThrough.game.connect(host, BreakThrough.D_PORT);
	}
	
	public static final BreakThrough getInstance()
	{
		if (BreakThrough.game == null)
		{
			BreakThrough.game = new BreakThrough();
		}
		return BreakThrough.game;
	}

	//============================================================================
	// INSTANCES ASSERTIONS
	//============================================================================
	private AMoveGenerator player;
	private IBoardEvaluators evaluator;
	
	private BreakThrough()
	{
		super();
	}
	
	private final void connect(final String host, final int port)
	{
		// Preparation done : Starting Client;
		try 
		{
			Client.connect(host, port);
		} 
		catch (IOException exc) 
		{
			System.out.println(exc + "\n\tCaused by : " + exc.getCause().getMessage());
		}
	}
	
	public final void disconnect()
	{
		// Preparation done : Starting Client;
		Client.disconnect();
	}
	
	public final void setUp(final int[][] board, final int player)
	{
		if(player == BreakThrough.WHITE)
		{
			this.player = new PlayerWhite(board);
			this.player.setOpponent(new PlayerBlack(board));
		}
		else
		{
			this.player = new PlayerBlack(board);
			this.player.setOpponent(new PlayerWhite(board));
		}
		this.evaluator = new EvalSimple();
		NegaMax.setUp(this.player, this.evaluator);
		System.out.println(this.player.print(0));
	}
	
	public final String nextMove(final String lastMove)
	{
		if(lastMove != "")
		{
			this.player.getOpponent().play(lastMove);
			System.out.println(this.player.print(0));
		}
		
		String temp = NegaMax.nextMove();
		System.out.println(this.player.print(0));
		return temp;
	}
	
	public final AMoveGenerator getPlayer()
	{
		return this.player;
	}
}

/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-31              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.moves;

import java.util.Arrays;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.ai.NegaMax;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.vars.BoardCoordinate;


public abstract class AMoveGenerator {
	//============================================================================
	// STATIC ASSERTIONS
	//============================================================================
	public static final int BORDER = 0xFF;
	public static final int PADDED_BORDER = 0x1FE;
	public static final int WEST   = 4;
	public static final int CENTER = 2;
	public static final int EAST   = 1;
	public static final int MOVES  = 7;
	public static final int DIR_SIDES  = 5;
	public static final int DIR_RIGHT  = 7;
	public static final int DIR_CENTER = 8;
	public static final int DIR_LEFT   = 9;
	public static final int KEY_POSITIONS    = 0;
	public static final int KEY_INDEX_ACTU   = 1;
	public static final int KEY_INDEX_FROM   = 2;
	public static final int KEY_INDEX_TO     = 3;
	public static final int KEY_INDEX_B_FROM = 4;
	public static final int KEY_INDEX_B_TO   = 5;
	public static final int KEY_X_COORD      = 6;
	public static final int KEY_MOVES        = 7;
	public static final int KEY_CLUSTER_SIZE = 8;
	//public static final int SKEY_SAVE_POS = 6;
	public static final long QUICK_MOD_8 = 7;
	private static final int FORMAT_LEN_S   = 4;
	private static final int FORMAT_LEN_M   = 7;
	private static final int FORMAT_POS_C1  = 0;
	private static final int FORMAT_POS_C2S = 2;
	private static final int FORMAT_POS_C2M = 5;
	private static final int FORMAT_POS_LEN = 2;
	private static final int FORMAT_OFFSET  = 1;
	
	public static final String drawBoard(final AMoveGenerator player, final AMoveGenerator opponent, final int depth, final int played)
	{
		String board = "";
		long mask = 0L;
		boolean whitePlay = (player.color == BreakThrough.WHITE);
		AMoveGenerator white, black;
		String w, b, m;
		w = (played == BreakThrough.WHITE)? "*W*|" : " W |";
		b = (played == BreakThrough.BLACK)? "*B*@" : " B @";
		if(whitePlay)
		{
			white = player;
			black = opponent;
		}
		else
		{
			white = opponent;
			black = player;
		}
		m = (0 != (white.arrayBranchValues[depth] & black.arrayBranchValues[depth])) ? " - capture" : "" ;
		System.out.print(player.color + w + b + " D:" + (depth/AMoveGenerator.KEY_CLUSTER_SIZE) + "(" + depth + ")" + m);
		for (int i = BreakThrough.BOARD; i > 0;)
		{
			i--;
			mask = 1L << i;
			if(whitePlay && 0 != (white.arrayBranchValues[depth] & black.arrayBranchValues[depth] & mask))
				board += 'c';
			else if (!whitePlay && 0 != (black.arrayBranchValues[depth] & white.arrayBranchValues[depth] & mask))
				board += 'y';
			else if((white.arrayBranchValues[depth] & mask) != 0)
				board += 'o';
			else if((black.arrayBranchValues[depth] & mask) != 0)
				board += 'x';
			else
				board += '.';
			if(i % BreakThrough.ROW_LENGTH == 0)
				board += '\n';
		}
		//return "";
		return "\n" + board;
	}
	
	//============================================================================
	// INSTANCE ASSERTIONS
	//============================================================================
	protected final long[] arrayBranchValues;
	protected AMoveGenerator opponent;
	private final int color;
	
	//private long playerMoveStart;
	//private Runnable calculator;
	
	public AMoveGenerator(final int[][] board, final int color)
	{
		this.color = color;
		this.arrayBranchValues = new long[AMoveGenerator.KEY_CLUSTER_SIZE * (NegaMax.ABSOLUTE_MAX_DEPTH + 1)];
		
		this.generate(board);
	}

	//============================================================================
	// CHILDREN ASSERTIONS
	//=====================
	protected abstract void movePeon(final int depth);
	protected abstract boolean peonMoves(final int depth);
	public abstract int evaluate(final IBoardEvaluators evaluator, final int depth);
	//============================================================================
	
	
	
	private final void generate(final int[][] board)
	{
		int square;
		int offset = BreakThrough.BOARD;
		for (int i = 0; i < BreakThrough.BOARD; i++)
		{
			offset--;
			square = board[((int) i / 8)][i % 8];
			if (square == 0)
			{
				continue;
			}
			else if (square == this.color)
			{
				this.arrayBranchValues[AMoveGenerator.KEY_POSITIONS] |= (1L << offset);
			}
		}
		//this.arrayBranchValues[AMoveGenerator.SKEY_SAVE_POS] = this.arrayBranchValues[AMoveGenerator.KEY_POSITIONS];
	}

	protected final void move(final int from, final int to, final int index)
	{
		this.arrayBranchValues[index] &= ~(1L << from);
		this.arrayBranchValues[index] |=  (1L << to);
		//long collision = this.arrayBranchValues[AMoveGenerator.KEY_INFO_POS + this.depthIndex] & this.opponent.arrayBranchValues[AMoveGenerator.KEY_INFO_POS + this.opponent.depthIndex];
		this.opponent.arrayBranchValues[index] &= ~(this.arrayBranchValues[index] & this.opponent.arrayBranchValues[index]);
		//System.out.println(this.print(index, this.color) + "\t" + BoardCoordinate.offset2coordinate(from) + BoardCoordinate.offset2coordinate(to));
	}
	

	public final void newBranch(final int depth)
	{
		this.arrayBranchValues[depth] = this.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE];
		//this.arrayBranchValues[depth + AMoveGenerator.KEY_POS_OPPONENT] = this.opponent.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE];
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU]   = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_FROM]   = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_TO]     = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_B_FROM] = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_B_TO]   = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD]      = 0L;
		this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES]        = 0L;
		this.opponent.arrayBranchValues[depth] = this.opponent.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE];
	}
	

	public final boolean nextMove(final int depth)
	{
		this.arrayBranchValues[depth] = this.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE];
		this.opponent.arrayBranchValues[depth] = this.opponent.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE];
		//this.opponent.arrayBranchValues[depth - AMoveGenerator.KEY_CLUSTER_SIZE] = this.arrayBranchValues[depth + AMoveGenerator.KEY_POS_OPPONENT];
		//System.out.println(Arrays.toString(this.arrayBranchValues));
		int i = depth + AMoveGenerator.KEY_INDEX_ACTU;
		int j = depth + AMoveGenerator.KEY_X_COORD;
		if(this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES] != 0)
		{
			this.movePeon(depth);
			return true;
		}
		while(this.arrayBranchValues[i] < BreakThrough.BOARD)
		{
			if(this.peonMoves(depth))
			{
				this.movePeon(depth);
				return true;
			}
			this.arrayBranchValues[i]++;
			this.arrayBranchValues[j] = ((this.arrayBranchValues[j] + 1) & AMoveGenerator.QUICK_MOD_8);
		}
		this.newBranch(depth);
		return false;
	}
	
	/**
	 * @param pos  :String Position expressed with format "Z9".
	 * @param move :String Move expressed with format "Z9".
	 */
	public final String play(final int from, final int to)
	{
		this.move(from, to, 0);
		//this.arrayBranchValues[AMoveGenerator.SKEY_SAVE_POS] = this.arrayBranchValues[AMoveGenerator.KEY_POSITIONS];
		System.out.println(this.print(0, this.color) + "\t" + BoardCoordinate.offset2coordinate(from) + BoardCoordinate.offset2coordinate(to));
		return BoardCoordinate.offset2coordinate(from) + BoardCoordinate.offset2coordinate(to);
	}
	
	/**
	 * @param move :String Move expressed with format "Z9Z9" or "Z9 - Z9".
	 */
	public final String play(final String move)
	{
		int i1, i2;
		String from, to;
		if (move.length() == AMoveGenerator.FORMAT_LEN_S)
		{
			i1 = AMoveGenerator.FORMAT_POS_C1;
			i2 = AMoveGenerator.FORMAT_POS_C2S;
		}
		else if (move.length() == AMoveGenerator.FORMAT_LEN_M)
		{
			i1 = AMoveGenerator.FORMAT_POS_C1;
			i2 = AMoveGenerator.FORMAT_POS_C2M;
		}
		else
		{	// Offset correction for crappy server message;
			i1 = AMoveGenerator.FORMAT_POS_C1 + AMoveGenerator.FORMAT_OFFSET;
			i2 = AMoveGenerator.FORMAT_POS_C2M + AMoveGenerator.FORMAT_OFFSET;
		}
		from = move.substring(i1, i1 + AMoveGenerator.FORMAT_POS_LEN);
		to   = move.substring(i2, i2 + AMoveGenerator.FORMAT_POS_LEN);
		
		this.move(BoardCoordinate.coordinate2offset(from), BoardCoordinate.coordinate2offset(to), 0);
		//this.arrayBranchValues[AMoveGenerator.SKEY_SAVE_POS] = this.arrayBranchValues[AMoveGenerator.KEY_POSITIONS];
		return from + to;
	}
	
	//============================================================================
	// ACCESSORS & UTILS METHODS
	//============================================================================
	public final void setOpponent(final AMoveGenerator opponent)
	{
		this.opponent = opponent;
		if(this != opponent.getOpponent())
			opponent.setOpponent(this);
	}
	
	public final AMoveGenerator getOpponent()
	{
		return this.opponent;
	}
	
	public final long getInfo(final int index)
	{
		return this.arrayBranchValues[index];
	}
	
	public final void setInfo(final int index, final long info)
	{
		System.out.println(Arrays.toString(this.arrayBranchValues));
		this.arrayBranchValues[index] = info;
	}
	
	public final void correctCluster()
	{
		//this.arrayBranchValues[AMoveGenerator.KEY_POSITIONS] = this.arrayBranchValues[AMoveGenerator.SKEY_SAVE_POS];
	}
	
	public final String print(final int depth)
	{
		return AMoveGenerator.drawBoard(this, this.opponent, depth, 0);
	}

	public final String print(final int depth, final int played)
	{
		return AMoveGenerator.drawBoard(this, this.opponent, depth, played);
	}
	
	@Override
	public String toString()
	{
		return ("[AMoveGenerator " + 
		                          "\tcolor="    + this.color +
		                          "\tclusters=" + Arrays.toString(this.arrayBranchValues) + 
		                          "]");
	}

	public int getColor() {
		return this.color;
	}
}

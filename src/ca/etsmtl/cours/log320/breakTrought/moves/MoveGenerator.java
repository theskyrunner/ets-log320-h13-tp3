/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-08              Class created;
 *  2013-03-09              Function generate() being created;
 *  2013-03-10              Function moves() being created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.moves;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.vars.BoardCoordinate;

public final class MoveGenerator
{
	//============================================================================
	// STATIC ASSERTIONS
	//============================================================================
	public static final int DIR_BLACK_EAST   = 0;
	public static final int DIR_BLACK_CENTER = 1;
	public static final int DIR_BLACK_WEST   = 2;
	public static final int DIR_WHITE_EAST   = 3;
	public static final int DIR_WHITE_CENTER = 4;
	public static final int DIR_WHITE_WEST   = 5;
	public static final long EAST_BORDER    = 0xFEFEFEFEFEFEFEFEL;
	public static final long WEST_BORDER    = 0x7F7F7F7F7F7F7F7FL;

	//============================================================================
	// INSTANCES ASSERTIONS
	//============================================================================
	private long posWhite;
	private long posBlack;
	private long posTempWhite;
	private long posTempBlack;
	protected long[] movesArray;
	/* Created for returning multiple values. Don't reference it*/
	private final long[] tempMovesArray;
	
	public MoveGenerator(final int[][] board)
	{
		super();
		this.posWhite       = 0L;
		this.posBlack       = 0L;
		this.movesArray     = new long[6];
		this.posTempWhite   = 0L;
		this.posTempBlack   = 0L;
		this.tempMovesArray = new long[6];
		this.generate(board);
	}

	private final void generate(final int[][] board) 
	{
		int square;
		int offset = BreakThrough.BOARD;
		for (int i = 0; i < BreakThrough.BOARD; i++)
		{
			offset--;
			square = board[((int) i / 8)][i % 8];
			if (square == 0)
			{
				continue;
			}
			else if (square == BreakThrough.WHITE)
			{
				//System.out.print("P[" + i + "] (p" + this.player + "|s" + square + ")::");
				//System.out.print(Long.toBinaryString(this.posPlayer) + "->");
				this.posTempWhite |= (1L << offset);
				//System.out.println(Long.toBinaryString(this.posPlayer));
			}
			else
			{
				//System.out.print("O[" + i + "] (p" + this.player + "|s" + square + ")::");
				//System.out.print(Long.toBinaryString(this.posOpponent) + "->");
				this.posTempBlack |= (1L << offset);
				//System.out.println(Long.toBinaryString(this.posOpponent));
			}
		}
		this.posWhite = this.posTempWhite;
		this.posBlack = this.posTempBlack;
		this.movesArray = this.moves(this.posWhite, this.posBlack).clone();
	}
	
	public final long[] moves(final long pWhite, final long pBlack)
	{
		//WHITE MOVES
		// Moving toward center
		this.tempMovesArray[MoveGenerator.DIR_WHITE_CENTER]  =  pWhite <<  BreakThrough.ROW_LENGTH;
		this.tempMovesArray[MoveGenerator.DIR_WHITE_CENTER] &= ~pWhite;
		this.tempMovesArray[MoveGenerator.DIR_WHITE_CENTER] &= ~pBlack;
		// Moving toward east
		this.tempMovesArray[MoveGenerator.DIR_WHITE_EAST]    = (pWhite & MoveGenerator.EAST_BORDER) <<  (BreakThrough.ROW_LENGTH - 1);
		this.tempMovesArray[MoveGenerator.DIR_WHITE_EAST]   &= ~pWhite;
		// Moving toward west
		this.tempMovesArray[MoveGenerator.DIR_WHITE_WEST]    = (pWhite & MoveGenerator.WEST_BORDER) <<  (BreakThrough.ROW_LENGTH + 1);
		this.tempMovesArray[MoveGenerator.DIR_WHITE_WEST]   &= ~pWhite;

		//BLACK MOVES
		// Moving toward center
		this.tempMovesArray[MoveGenerator.DIR_BLACK_CENTER]  =  pBlack >>> BreakThrough.ROW_LENGTH;
		this.tempMovesArray[MoveGenerator.DIR_BLACK_CENTER] &= ~pBlack;
		this.tempMovesArray[MoveGenerator.DIR_BLACK_CENTER] &= ~pWhite;
		// Moving toward east
		this.tempMovesArray[MoveGenerator.DIR_BLACK_EAST]    = (pBlack & MoveGenerator.EAST_BORDER) >>> (BreakThrough.ROW_LENGTH + 1);
		this.tempMovesArray[MoveGenerator.DIR_BLACK_EAST]   &= ~pBlack;
		// Moving toward west
		this.tempMovesArray[MoveGenerator.DIR_BLACK_WEST]    = (pBlack & MoveGenerator.WEST_BORDER) >>> (BreakThrough.ROW_LENGTH - 1);
		this.tempMovesArray[MoveGenerator.DIR_BLACK_WEST]   &= ~pBlack;
		
		return this.tempMovesArray;
	}
	
	public final String updateBoard(final int from, final int to, final int player)
	{
		if(from == 0)
		{			
			return "";
		}
		else if(player == BreakThrough.WHITE)
		{
			this.posWhite &= ~(1L << from);
			this.posWhite |=  (1L << to);
			this.posBlack &= ~(1L << to);
		}
		else
		{
			this.posBlack &= ~(1L << from);
			this.posBlack |=  (1L << to);
			this.posWhite &= ~(1L << to);
		}
		this.posTempWhite = this.posWhite;
		this.posTempBlack = this.posBlack;
		this.moves(this.posTempWhite, this.posTempBlack);
		this.movesArray = this.moves(this.posWhite, this.posBlack).clone();
		return BoardCoordinate.offset2coordinate(from) + BoardCoordinate.offset2coordinate(to);
	}
	
	//============================================================================
	// Test methods
	//============================================================================	
	public final long getPosWhite()
	{
		return this.posTempWhite;
	}
	public final long getPosBlack()
	{
		return this.posTempBlack;
	}
	public final long getMoveWhite(final int direction)
	{
		return this.tempMovesArray[direction];
	}
	public final long getMoveBlack(final int direction)
	{
		return this.tempMovesArray[direction];
	}
	
	@Override
	public String toString()
	{
		return ("[MoveGenerator " + 
				              "\tWP=" + Long.toBinaryString(this.posTempWhite) +
				              "\tBP=" + Long.toBinaryString(this.posTempBlack) + 
				              "\tWM[E]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_WHITE_EAST])   + 
				              "\tWM[C]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_WHITE_CENTER]) + 
				              "\tWM[W]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_WHITE_WEST])   + 
				              "\tBM[E]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_BLACK_EAST])   + 
				              "\tBM[C]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_BLACK_CENTER]) + 
				              "\tBM[W]=" + Long.toBinaryString(this.tempMovesArray[MoveGenerator.DIR_BLACK_WEST])   + "]");
	}
}
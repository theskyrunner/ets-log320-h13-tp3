/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-04-01              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.moves;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;

public class PlayerWhite extends AMoveGenerator
{
	public PlayerWhite(final int[][] board)
	{
		super(board, BreakThrough.WHITE);
	}

	@Override
	protected final boolean peonMoves(final int depth)
	{
		if((this.arrayBranchValues[depth] & (1L << this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU])) == 0)
			return false;
		//if((0 != (this.arrayBranchValues[depth] & IBoardEvaluators.ROW_8)) || (0 != (this.opponent.arrayBranchValues[depth] & IBoardEvaluators.ROW_1)))
		if(0 != (this.arrayBranchValues[depth] & IBoardEvaluators.ROW_8))
		{
			this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU]++;
			this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD] = ((this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD] + 1) & AMoveGenerator.QUICK_MOD_8);
			return true;
		}
		this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES]  = (AMoveGenerator.MOVES & (AMoveGenerator.PADDED_BORDER >> this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD]));
		this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES] &= ((~this.arrayBranchValues[depth + AMoveGenerator.KEY_POSITIONS]) >>> (this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU] + BreakThrough.ROW_LENGTH - 1));
		if(0 != (1 & (this.opponent.arrayBranchValues[depth + AMoveGenerator.KEY_POSITIONS] >>> (this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU] + BreakThrough.ROW_LENGTH))))
			this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES] &= AMoveGenerator.DIR_SIDES;
		
		if(0 == this.arrayBranchValues[depth + AMoveGenerator.KEY_MOVES])
			return false;
		
		//System.out.print("["+depth+"|"+this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_FROM]+"|"+this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_FROM] + "->\t");
		this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_FROM] = this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU];
		
		return true;
	}

	@Override
	protected final void movePeon(final int depth)
	{
		int indMove = depth + AMoveGenerator.KEY_MOVES;
		int indActu = depth + AMoveGenerator.KEY_INDEX_ACTU;
		int indTo = depth + AMoveGenerator.KEY_INDEX_TO;
		if((this.arrayBranchValues[indMove] & AMoveGenerator.CENTER) != 0)
		{
			this.arrayBranchValues[indMove] &= ~AMoveGenerator.CENTER;
			this.arrayBranchValues[indTo] = this.arrayBranchValues[indActu] + AMoveGenerator.DIR_CENTER;
			this.move(((int) this.arrayBranchValues[indActu]), ((int) (this.arrayBranchValues[indTo])), depth);
		}
		else if((this.arrayBranchValues[indMove] & AMoveGenerator.WEST) != 0)
		{
			this.arrayBranchValues[indMove] &= ~AMoveGenerator.WEST;
			this.arrayBranchValues[indTo] = this.arrayBranchValues[indActu] + AMoveGenerator.DIR_LEFT;
			this.move(((int) this.arrayBranchValues[indActu]), ((int) (this.arrayBranchValues[indTo])), depth);
		}
		else if((this.arrayBranchValues[indMove] & 1) != 0)
		{
			this.arrayBranchValues[indMove] &= ~AMoveGenerator.EAST;
			this.arrayBranchValues[indTo] = this.arrayBranchValues[indActu] + AMoveGenerator.DIR_RIGHT;
			this.move(((int) this.arrayBranchValues[indActu]), ((int) (this.arrayBranchValues[indTo])), depth);
		}
		if(this.arrayBranchValues[indMove] == 0)
		{
			this.arrayBranchValues[indActu]++;
			this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD] = ((this.arrayBranchValues[depth + AMoveGenerator.KEY_X_COORD] + 1) & AMoveGenerator.QUICK_MOD_8);
		}
		//System.out.print("\t["+this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_FROM]+"|"+this.arrayBranchValues[depth + AMoveGenerator.KEY_INDEX_ACTU]+"]\n");
	}

	@Override
	public final int evaluate(final IBoardEvaluators evaluator, final int depth)
	{
		return evaluator.evalBoard(this, depth);
	}
}

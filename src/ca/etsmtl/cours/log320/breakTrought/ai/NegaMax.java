/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-04-03              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.ai;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.evals.EvalSimple;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.moves.AMoveGenerator;

public class NegaMax {
	private static IBoardEvaluators evaluator;
	private static AMoveGenerator player;
	//private static AMoveGenerator playing;
	private final static int INIT_DEPTH         = 1;
	private final static int INIT_MAX_DEPTH     = 2;
	public  final static int ABSOLUTE_MAX_DEPTH = 12;
	private static int[] bestMove;
	private static int[] tempMove;

	private static int maxDepth = NegaMax.INIT_MAX_DEPTH;
	private static boolean hasTime;
	
	public static final void setUp(final AMoveGenerator player, final IBoardEvaluators evaluator)
	{
		NegaMax.evaluator = evaluator;
		NegaMax.player = player;
	}
	
	public static synchronized final String nextMove()
	{
		NegaMax.bestMove = new int[2];
		NegaMax.tempMove = new int[2];
		long playerMoveStart = System.currentTimeMillis(); // DEPRECATED
		long playerMoveStop = playerMoveStart + BreakThrough.TIME_MAX;
		//System.out.println(playerMoveStop + " - MAX");
		
		Thread calculator = new Thread()
		{
			@Override
			public void run() {
				NegaMax.hasTime = true;
				NegaMax.resetMaxDepth();
				while(NegaMax.hasTime) {
					NegaMax.findNextMove();
					NegaMax.incrementMaxDepth();
					NegaMax.bestMove = NegaMax.tempMove;
					NegaMax.tempMove = NegaMax.bestMove.clone();
				}
				//Thread.currentThread().interrupt();
			}
			
			@Override
			public void interrupt()
			{
				NegaMax.hasTime = false;
				System.out.println((System.currentTimeMillis()) + " - INTERRUPT");
				super.interrupt();
			}
		};
		
		// START CALCUL (RUNNABLE)
		calculator.start();
		System.out.println("THREADS " + Thread.activeCount());
		
		// STOP CLIENT THREAD
		// Pausing this Thread to give CPU time to the other one;
		try {
			Thread.sleep(BreakThrough.TIME_SLEEP);
		} catch (InterruptedException e) {
			System.out.println("Thread Error");
			BreakThrough.getInstance().disconnect();
		}
		while(playerMoveStop > System.currentTimeMillis() + BreakThrough.TIME_PADDING)
		{
			//System.out.println(System.currentTimeMillis() + " -");
			try {
				Thread.sleep(BreakThrough.TIME_INTERVAL);
			} catch (InterruptedException e) {
				System.out.println("Thread Error");
				BreakThrough.getInstance().disconnect();
			}
		}
		
		// STOP CALCUL (RUNNABLE)
		NegaMax.hasTime = false;
		calculator.interrupt();
		System.out.println((System.currentTimeMillis() - playerMoveStart) + " - TIME");
		
		return player.play(NegaMax.bestMove[0], NegaMax.bestMove[1]);
	}
	
	
	public static final void findNextMove()
	{
		int score = 0;
		int alpha = Integer.MIN_VALUE;
		int beta  = Integer.MAX_VALUE;
		NegaMax.player.correctCluster();
		NegaMax.player.getOpponent().correctCluster();
		NegaMax.player.newBranch(NegaMax.INIT_DEPTH * AMoveGenerator.KEY_CLUSTER_SIZE);
		//while(NegaMax.hasTime && NegaMax.player.nextMove(NegaMax.INIT_DEPTH * AMoveGenerator.KEY_CLUSTER_SIZE, (NegaMax.INIT_DEPTH - 1) * AMoveGenerator.KEY_CLUSTER_SIZE))
		while(NegaMax.hasTime && NegaMax.player.nextMove(NegaMax.INIT_DEPTH * AMoveGenerator.KEY_CLUSTER_SIZE))
		{
			score = -NegaMax.iterateNextMove(NegaMax.player.getOpponent(), NegaMax.INIT_DEPTH + 1, -beta, -alpha);
			//System.out.print("SCORE\t" + score);
			if(score >= alpha)
			{
				NegaMax.tempMove[0] = ((int) NegaMax.player.getInfo(AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_FROM));	
				NegaMax.tempMove[1] = ((int) NegaMax.player.getInfo(AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_TO));	
				alpha = score;
				//System.out.println("\t("+ BoardCoordinate.offset2coordinate(NegaMax.tempMove[0]) + BoardCoordinate.offset2coordinate(NegaMax.tempMove[1]) +") - TAKE");
			}
		}
	}
	
	//=====================================================================================================
	// Methode Basée sur le pseudo code de l'Algorithme NegaMax
	// http://en.wikipedia.org/wiki/Negamax
	public static final int iterateNextMove(final AMoveGenerator playing, final int depth, int alpha, final int beta)
	{
		if(NegaMax.maxDepth <= depth)
		{
			return playing.evaluate(NegaMax.evaluator, (depth-1) * AMoveGenerator.KEY_CLUSTER_SIZE);
		}
		else
		{
			int score = 0;
			playing.newBranch(depth * AMoveGenerator.KEY_CLUSTER_SIZE);
			//while(NegaMax.hasTime & playing.nextMove(depth * AMoveGenerator.KEY_CLUSTER_SIZE, (depth - 2) * AMoveGenerator.KEY_CLUSTER_SIZE))
			while(NegaMax.hasTime & playing.nextMove(depth * AMoveGenerator.KEY_CLUSTER_SIZE))
			{
				score = -NegaMax.iterateNextMove(playing.getOpponent(), depth + 1, -beta, -alpha);
				if(beta < score)
				{
					NegaMax.player.setInfo(depth * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_FROM, NegaMax.player.getInfo((depth+1) * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_FROM));	
					NegaMax.player.setInfo(depth * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_TO,   NegaMax.player.getInfo((depth+1) * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_TO));
					return score;
				}
				else if (alpha <= score)
				{
					alpha = score;
				}
			}
			NegaMax.player.setInfo(depth * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_FROM, NegaMax.player.getInfo((depth+1) * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_FROM));	
			NegaMax.player.setInfo(depth * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_B_TO,   NegaMax.player.getInfo((depth+1) * AMoveGenerator.KEY_CLUSTER_SIZE + AMoveGenerator.KEY_INDEX_TO));
			return score;
		}
	}
	// Fin du code emprunté
	//=====================================================================================================
	
	public static final void incrementMaxDepth()
	{
		int newDepth = NegaMax.maxDepth + 1;
		if(newDepth <= NegaMax.ABSOLUTE_MAX_DEPTH)
		{			
			NegaMax.maxDepth = newDepth;
			System.out.println("DEPTH :: " + NegaMax.maxDepth + " - DEEPER");
		}
		else
		{
			System.out.println("DEPTH :: " + NegaMax.maxDepth + " - MAX REACHED");
			NegaMax.hasTime = false;
			//Thread.currentThread().interrupt();
		}
	}
	
	public static final void resetMaxDepth()
	{
		NegaMax.maxDepth = NegaMax.INIT_MAX_DEPTH;
		System.out.println("DEPTH :: " + NegaMax.maxDepth + " - RESETED");
	}
	
}

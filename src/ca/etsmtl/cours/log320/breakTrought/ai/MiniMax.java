/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-06              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.ai;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.moves.MoveGenerator;

public class MiniMax {

	private static MoveGenerator mvgen;
	private static IBoardEvaluators evaluator;
	private final static int START_DEPTH = 4;
	private final static int MAX_DEPTH   = 15;

	public final static int KEY_SCORE    = 0;
	public final static int KEY_POSITION = 1;
	public final static int KEY_MOVE     = 2;
	private final static int[] resArray = new int[3];
	private static int player;
	private static int depth = MiniMax.START_DEPTH;
	
	public static final String nextMove(final MoveGenerator mvgen, final IBoardEvaluators evaluator, final int player)
	{
		MiniMax.mvgen = mvgen;
		MiniMax.evaluator = evaluator;
		MiniMax.player = player;
		//MiniMax.resArray[MiniMax.KEY_ALPHA] = Integer.MIN_VALUE;
		//MiniMax.resArray[MiniMax.KEY_BETA]  = Integer.MAX_VALUE;
		System.out.println("WHITE:" + Long.toBinaryString(mvgen.getPosWhite()));
		System.out.println("BLACK:" + Long.toBinaryString(mvgen.getPosBlack()));
		if(player == BreakThrough.WHITE)
			MiniMax.computeNextWhiteMove(mvgen.getPosWhite(), mvgen.getPosBlack(), true, Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
		else 
			MiniMax.computeNextBlackMove(mvgen.getPosBlack(), mvgen.getPosWhite(), true, Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
		
		System.out.println(evaluator);
		
		return mvgen.updateBoard(MiniMax.resArray[MiniMax.KEY_POSITION], MiniMax.resArray[MiniMax.KEY_MOVE], player);
	}
	
	private static final int computeNextWhiteMove(final long playerPositions, final long opponentPositions, final boolean isMax, final int pAlpha, final int pBeta, final int depth)
	{
		if(depth == MiniMax.depth)
		{
			return (int)(Math.random() * 100);
			//return evaluator.evalBoard(null);
		}
		
		long move, pos;
		int  i, peonPos, end, max, score;
		System.out.println("----");
		if(isMax)
		{
			System.out.println("[MAX []]");
			int alpha = Integer.MIN_VALUE; // Alpha for this node
			score = Integer.MIN_VALUE;
			end = BreakThrough.BOARD;
			MAX:
			for(int d = -1; d < 2; d++)
			{
				move = mvgen.getMoveWhite(MoveGenerator.DIR_WHITE_CENTER + d);
				for(i = BreakThrough.ROW_LENGTH; i < end; i++)
				{
					if(((move >>> i) & 1) == 0)
						continue;
					peonPos = i - (BreakThrough.ROW_LENGTH + d);
					pos   = (playerPositions | (1L << i)) & ~(1L << peonPos);
					max =  Math.max(score, MiniMax.computeNextWhiteMove(pos, opponentPositions, !isMax, Math.max(alpha, pAlpha), pBeta, depth + 1));
					
					if(depth == 0 && max != score)
					{
						MiniMax.resArray[MiniMax.KEY_MOVE] = i;
						MiniMax.resArray[MiniMax.KEY_POSITION] = peonPos;
					}
					score = max;
					alpha =  Math.max(score, alpha);
					if(alpha >= pBeta )					
						break MAX;
				}
			}
		}
		else
		{
			System.out.println("[min []]");
			int beta = Integer.MAX_VALUE; // Alpha for this node
			score = Integer.MAX_VALUE;
			end = BreakThrough.BOARD - BreakThrough.ROW_LENGTH;
			MIN:
			for(int d = -1; d < 2; d++)
			{
				move = mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_CENTER + d);
				for(i = BreakThrough.ROW_LENGTH; i < end; i++)
				{
					if(((move >>> i) & 1) == 0)
						continue;
					peonPos = i + (BreakThrough.ROW_LENGTH - d);
					pos   = (opponentPositions | (1L << i)) & ~(1L << peonPos);
					score =  Math.min(score, MiniMax.computeNextWhiteMove(playerPositions, pos, !isMax, pAlpha, Math.min(beta, pBeta), depth + 1));
					beta =  Math.min(score, beta);
					if(beta <= pAlpha)					
						break MIN;
				}
			}
		}
		System.out.println("\nreturned="+score);
		return score;
	}
	
	private static final int computeNextBlackMove(final long playerPositions, final long opponentPositions, final boolean isMax, final int pAlpha, final int pBeta, final int depth)
	{
		return 0;
	}
	
	public static void incrementDepth()
	{
		MiniMax.depth++;
	}
	
	public static void incrementDepth(final int step)
	{
		MiniMax.depth += step;
		if(MiniMax.depth > MiniMax.MAX_DEPTH)
			MiniMax.depth = MiniMax.MAX_DEPTH;
	}
	
	public static void resetDepth()
	{
		MiniMax.depth = MiniMax.START_DEPTH;
	}
}

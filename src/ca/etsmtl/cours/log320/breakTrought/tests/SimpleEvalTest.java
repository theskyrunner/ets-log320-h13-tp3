/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-08              Class created;
 *  2013-03-09              Function generate() being test;
 *  2013-03-10              Function moves() being test;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.tests;

import junit.framework.TestCase;
import ca.etsmtl.cours.log320.breakTrought.ai.NegaMax;
import ca.etsmtl.cours.log320.breakTrought.evals.EvalSimple;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;

/**
 * JUnit test class.
 * @author nicolas
 */
public final class SimpleEvalTest extends TestCase
{
	//============================================================================
	// TEST PREPARATION
	//============================================================================
	public static final long TEST_POS_START_WHITE       = 0xFFFF000000000000L;
	public static final long TEST_POS_START_BLACK       = 0x000000000000FFFFL;
	public static final long TEST_START_MOVE_DOWNEAST   = 0x00007F0000000000L;
	public static final long TEST_START_MOVE_DOWNCENTER = 0x0000FF0000000000L;
	public static final long TEST_START_MOVE_DOWNWEST   = 0x0000FE0000000000L;
	public static final long TEST_START_MOVE_UPEAST     = 0x00000000007F0000L;
	public static final long TEST_START_MOVE_UPCENTER   = 0x0000000000FF0000L;
	public static final long TEST_START_MOVE_UPWEST     = 0x0000000000FE0000L;

	public static final long TEST_MOVE_1B_EAST   = 0x0000020200040000L;
	public static final long TEST_MOVE_1B_CENTER = 0x0000000400000000L;
	public static final long TEST_MOVE_1B_WEST   = 0x0000080800100000L; 
	private final static int[][][] boards = {
		{	// [0]
			// Starting Board;
			{2,2,2,2,2,2,2,2},
			{2,2,2,2,2,2,2,2},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{4,4,4,4,4,4,4,4},
			{4,4,4,4,4,4,4,4}
		},
		{	// [1]
			// moves possibilities
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,0,0,0,0,0,0,0}
		},
		{	// [2]
			// White player has win
			{0,4,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,0,0,0,0,0,0,0}
		},
		{	// [3]
			// Black player has win
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,2,0,0,0,0,0,0}
		},
		{	// [4]
			// Black player shall eat
			// White player shall win
			{2,0,2,0,0,0,0,0},
			{0,0,0,4,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,0,0,0,0,0,0,0}
		},
		{	// [5]
			// White player shall eat
			// Black player shall win
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,2,0,0,0,0,0,4},
			{4,4,4,0,0,0,0,0}
		}
	};
	
	@Override
	protected final void setUp() throws Exception
	{
		super.setUp();
	}

	//============================================================================
	// TESTS
	//============================================================================
	
	
	public void testSimpleBoards()
	{
		PlayerBlack playerB;
		PlayerWhite playerW;
		EvalSimple evaluator = new EvalSimple();
		boolean valid ;
		String res;
		
		//===================================================
		// CASE 1a
		//--------
		valid = true;
		res = null;
		playerB = new PlayerBlack(SimpleEvalTest.boards[4]);
		playerW = new PlayerWhite(SimpleEvalTest.boards[4]);
		playerW.setOpponent(playerB);
		// Testing
		NegaMax.setUp(playerB, evaluator);
		res = NegaMax.nextMove();
		valid = valid && (res != null);
		valid = valid && (res.indexOf("C8D7") != -1);
		System.out.println("R:" + res);
		SimpleEvalTest.assertTrue(valid);
		
		// CASE 1b
		//--------
		playerB = new PlayerBlack(SimpleEvalTest.boards[4]);
		playerW = new PlayerWhite(SimpleEvalTest.boards[4]);
		playerW.setOpponent(playerB);
		valid = true;
		res = null;
		// Testing
		NegaMax.setUp(playerW, evaluator);
		res = NegaMax.nextMove();
		valid = valid && (res != null);
		valid = valid && (res.indexOf("D7") != -1);
		System.out.println("R:" + res);
		SimpleEvalTest.assertTrue(valid);
		
		//===================================================
		// CASE 2a
		//--------
		valid = true;
		res = null;
		playerB = new PlayerBlack(SimpleEvalTest.boards[5]);
		playerW = new PlayerWhite(SimpleEvalTest.boards[5]);
		playerW.setOpponent(playerB);
		// Testing
		NegaMax.setUp(playerB, evaluator);
		res = NegaMax.nextMove();
		valid = valid && (res != null);
		valid = valid && (res.indexOf("B2") != -1);
		System.out.println("R:" + res);
		SimpleEvalTest.assertTrue(valid);
		
		// CASE 2b
		//--------
		valid = true;
		res = null;
		playerB = new PlayerBlack(SimpleEvalTest.boards[5]);
		playerW = new PlayerWhite(SimpleEvalTest.boards[5]);
		playerW.setOpponent(playerB);
		// Testing
		NegaMax.setUp(playerW, evaluator);
		res = NegaMax.nextMove();
		valid = valid && (res != null);
		valid = valid && (res.indexOf("B2") != -1);
		System.out.println("R:" + res);
		SimpleEvalTest.assertTrue(valid);
	}
}

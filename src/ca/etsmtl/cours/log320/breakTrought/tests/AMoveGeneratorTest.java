/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-08              Class created;
 *  2013-03-09              Function generate() being test;
 *  2013-03-10              Function moves() being test;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.tests;

import junit.framework.TestCase;
import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.ai.NegaMax;
import ca.etsmtl.cours.log320.breakTrought.evals.EvalRandom;
import ca.etsmtl.cours.log320.breakTrought.evals.IBoardEvaluators;
import ca.etsmtl.cours.log320.breakTrought.moves.AMoveGenerator;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;

/**
 * JUnit test class.
 * @author nicolas
 */
public final class AMoveGeneratorTest extends TestCase
{
	//============================================================================
	// TEST PREPARATION
	//============================================================================
	private final static int[][][] boards = {
		{	// Starting Board;
			{2,2,2,2,2,2,2,2},
			{2,2,2,2,2,2,2,2},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{4,4,4,4,4,4,4,4},
			{4,4,4,4,4,4,4,4}
		},
		{
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,0,0,0,0,0,0,0}
		}
	};
	
	private String moveTest;
	public AMoveGenerator amvgenB;
	public AMoveGenerator amvgenW;
	public PlayerBlack playerB;
	public PlayerWhite playerW;
	
	
	@Override
	protected final void setUp() throws Exception
	{
		super.setUp();
		this.amvgenB = new AMoveGenerator(AMoveGeneratorTest.boards[0], BreakThrough.BLACK)
		{
			@Override
			protected final boolean peonMoves(final int depth)
			{
				long i = 0;
				while(++i > 0)
				{
					if(i % 100000000 == 0 && i > 600000000)
						System.out.println(i);
				}
				return false;
			}

			@Override
			protected final void movePeon(final int depth) {}

			@Override
			public final int evaluate(final IBoardEvaluators evaluator, final int depth) {return 0;}
		};

		this.amvgenW = new AMoveGenerator(AMoveGeneratorTest.boards[0], BreakThrough.WHITE)
		{
			@Override
			protected final boolean peonMoves(final int depth) {return false;}

			@Override
			protected final void movePeon(final int depth) {}

			@Override
			public final int evaluate(final IBoardEvaluators evaluator, final int depth) {return 0;}
		};

		this.playerB = new PlayerBlack(AMoveGeneratorTest.boards[0]);
		this.playerW = new PlayerWhite(AMoveGeneratorTest.boards[0]);
		
		this.amvgenB.setOpponent(this.amvgenW);
		this.playerB.setOpponent(this.playerW);
	}

	//============================================================================
	// TESTS
	//============================================================================
	public final void testInit()
	{
		AMoveGeneratorTest.assertTrue(this.amvgenB == this.amvgenW.getOpponent());
		AMoveGeneratorTest.assertTrue(this.amvgenW == this.amvgenB.getOpponent());
		AMoveGeneratorTest.assertTrue(this.amvgenW == this.amvgenB.getOpponent());
		AMoveGeneratorTest.assertTrue(this.amvgenW == this.amvgenB.getOpponent());
	}
	
	public final void testPlay()
	{
		//Impossible moveset - Just for testing
		this.moveTest = "H7H2";
		AMoveGeneratorTest.assertEquals(amvgenB.play(this.moveTest), "H7H2");
		AMoveGeneratorTest.assertEquals(playerB.play(this.moveTest), "H7H2");
		System.out.println(this.playerW.print(0));

		
		this.moveTest = "G1 - H2";
		AMoveGeneratorTest.assertEquals(amvgenW.play(this.moveTest), "G1H2");
		AMoveGeneratorTest.assertEquals(playerW.play(this.moveTest), "G1H2");
		System.out.println(this.playerW.print(0));
		
		this.moveTest = " B8 - A2";
		AMoveGeneratorTest.assertEquals(amvgenB.play(this.moveTest), "B8A2");
		AMoveGeneratorTest.assertEquals(playerB.play(this.moveTest), "B8A2");
		System.out.println(this.playerW.print(0));

		//System.out.println(this.amvgenB);
		//System.out.println(AMoveGenerator.drawBoard(this.amvgenW, this.amvgenB, 0));
	}

	public final void testMovements()
	{
		// Means that nextMove() finds no moves in time !!!
		NegaMax.setUp(playerW, new EvalRandom());
		AMoveGeneratorTest.assertFalse(NegaMax.nextMove().equalsIgnoreCase("H1H1"));
	}
}

/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-06              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.tests;

import ca.etsmtl.cours.log320.breakTrought.vars.BoardCoordinate;
import junit.framework.TestCase;

/**
 * Entry point of the Application.
 * @author Nicolas
 */
public class BoardCoordinateTest extends TestCase
{
	public final void testCoordinates()
	{
		BreakTroughtTest.assertEquals(BoardCoordinate.offset2coordinate(0), "H1");
		BreakTroughtTest.assertEquals(BoardCoordinate.coordinate2offset("H1"), 0);
		BreakTroughtTest.assertEquals(BoardCoordinate.offset2coordinate(7), "A1");
		BreakTroughtTest.assertEquals(BoardCoordinate.coordinate2offset("A1"), 7);
		BreakTroughtTest.assertEquals(BoardCoordinate.offset2coordinate(56), "H8");
		BreakTroughtTest.assertEquals(BoardCoordinate.coordinate2offset("H8"), 56);
		BreakTroughtTest.assertEquals(BoardCoordinate.offset2coordinate(63), "A8");
		BreakTroughtTest.assertEquals(BoardCoordinate.coordinate2offset("A8"), 63);
	}
}

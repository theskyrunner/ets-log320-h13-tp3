/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-08              Class created;
 *  2013-03-09              Function generate() being test;
 *  2013-03-10              Function moves() being test;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.tests;

import junit.framework.TestCase;
import ca.etsmtl.cours.log320.breakTrought.moves.MoveGenerator;

/**
 * JUnit test class.
 * @author nicolas
 */
public final class MoveGeneratorTest extends TestCase
{
	//============================================================================
	// TEST PREPARATION
	//============================================================================
	public static final long TEST_POS_START_WHITE       = 0xFFFF000000000000L;
	public static final long TEST_POS_START_BLACK       = 0x000000000000FFFFL;
	public static final long TEST_START_MOVE_DOWNEAST   = 0x00007F0000000000L;
	public static final long TEST_START_MOVE_DOWNCENTER = 0x0000FF0000000000L;
	public static final long TEST_START_MOVE_DOWNWEST   = 0x0000FE0000000000L;
	public static final long TEST_START_MOVE_UPEAST     = 0x00000000007F0000L;
	public static final long TEST_START_MOVE_UPCENTER   = 0x0000000000FF0000L;
	public static final long TEST_START_MOVE_UPWEST     = 0x0000000000FE0000L;

	public static final long TEST_MOVE_1B_EAST   = 0x0000020200040000L;
	public static final long TEST_MOVE_1B_CENTER = 0x0000000400000000L;
	public static final long TEST_MOVE_1B_WEST   = 0x0000080800100000L; 
	private final static int[][][] boards = {
		{	// Starting Board;
			{2,2,2,2,2,2,2,2},
			{2,2,2,2,2,2,2,2},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{4,4,4,4,4,4,4,4},
			{4,4,4,4,4,4,4,4}
		},
		{
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,2,0,0},
			{0,0,0,0,0,2,4,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,2,0,0,0},
			{0,0,0,0,4,0,0,0},
			{0,0,0,0,0,0,0,4},
			{0,0,0,0,0,0,0,0}
		}
	};
	
	@Override
	protected final void setUp() throws Exception
	{
		super.setUp();
	}

	//============================================================================
	// TESTS
	//============================================================================
	public final void testInitiation()
	{
		MoveGenerator mvgen = new MoveGenerator(MoveGeneratorTest.boards[0]);
		//System.out.println(mvgen.toString());
		//Testing found position
		MoveGeneratorTest.assertEquals(mvgen.getPosWhite(), MoveGeneratorTest.TEST_POS_START_BLACK);
		MoveGeneratorTest.assertEquals(mvgen.getPosBlack(), MoveGeneratorTest.TEST_POS_START_WHITE);
		
		//Testing found player move (before first move)
		MoveGeneratorTest.assertEquals(mvgen.getMoveWhite(MoveGenerator.DIR_WHITE_CENTER), MoveGeneratorTest.TEST_START_MOVE_UPCENTER);
		MoveGeneratorTest.assertEquals(mvgen.getMoveWhite(MoveGenerator.DIR_WHITE_EAST)  , MoveGeneratorTest.TEST_START_MOVE_UPEAST);
		MoveGeneratorTest.assertEquals(mvgen.getMoveWhite(MoveGenerator.DIR_WHITE_WEST)  , MoveGeneratorTest.TEST_START_MOVE_UPWEST);
		
		//Testing found opponent move (before first move)
		MoveGeneratorTest.assertFalse(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_CENTER) == -1L);
		MoveGeneratorTest.assertFalse(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_EAST)   == -1L);
		MoveGeneratorTest.assertFalse(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_WEST)   == -1L);
				
		MoveGeneratorTest.assertTrue(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_CENTER)  == MoveGeneratorTest.TEST_START_MOVE_DOWNCENTER);
		MoveGeneratorTest.assertTrue(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_EAST)    == MoveGeneratorTest.TEST_START_MOVE_DOWNEAST);
		MoveGeneratorTest.assertTrue(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_WEST)    == MoveGeneratorTest.TEST_START_MOVE_DOWNWEST);
	}
	
	public final void testGenMove()
	{
		MoveGenerator mvgen = new MoveGenerator(MoveGeneratorTest.boards[1]);
		MoveGeneratorTest.assertEquals(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_EAST)  , MoveGeneratorTest.TEST_MOVE_1B_EAST  );
		MoveGeneratorTest.assertEquals(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_CENTER), MoveGeneratorTest.TEST_MOVE_1B_CENTER);
		MoveGeneratorTest.assertEquals(mvgen.getMoveBlack(MoveGenerator.DIR_BLACK_WEST)  , MoveGeneratorTest.TEST_MOVE_1B_WEST  );
	}
}

/*******************************************************************************
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Project :                BreakTrought
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                2013-01-14
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  Unknown                 Class created;
 *  2013-03-08              Static assertions moved to instance;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


/**
 * Serves as a player client for BreakTrought's server
 * 
 * @author UNKNOWN
 * @author Nicolas
 * This code is distributed in the course LOG320 for the 
 * third practical work as starting material.
 * This code is subject to copyrights.
 */
final class Client
{
	private final static String ID = "6c0d43a8-3367-44f9-a0dc-74b06ab586a4";
	private static Socket myClient;
	private static BufferedInputStream input;
	private static BufferedOutputStream output;
	private static BufferedReader console;
	private static boolean isConnected;
	//private String nextMove;
	
	public static final void connect(final String host, final int port) throws IOException
	{
	    int[][] board = new int[8][8];
	    String lastMove = "";
	    Client.isConnected = true;
		try {
			Client.myClient = new Socket(host, port);
			Client.input    = new BufferedInputStream(Client.myClient.getInputStream());
			Client.output   = new BufferedOutputStream(Client.myClient.getOutputStream());
			Client.console  = new BufferedReader(new InputStreamReader(System.in));
			byte[] aBuffer;
			int size, x=0, y=0;
			String s, move;
			String[] boardValues;
			if(host != BreakThrough.D_HOST)
			{
				Client.output.write(Client.ID.getBytes(), 0, Client.ID.length());
				Client.output.flush();
			}
		   	while(Client.isConnected){
				char cmd = 0;
	            cmd = (char)input.read();
	            
	            // Start as white player	  
	            switch (cmd)
	            {
	            
	            	case '1':
		            	System.out.println("New game! Your playing as White. Enter your move : ");
		                aBuffer = new byte[1024];
						
						size = input.available();
						//System.out.println("size " + size);
						input.read(aBuffer,0,size);
		                s = new String(aBuffer).trim();
		                boardValues = s.split(" ");
		                x = 0;
		                y = 0;
		                for(int i = 0; i < boardValues.length; i++){
		                    board[y][x] = Integer.parseInt(boardValues[i]);
		                    x++;
		                    if(x == 8){
		                        x = 0;
		                        y++;
		                    }
		                }
		            	BreakThrough.getInstance().setUp(board, BreakThrough.WHITE);
		                move = null;
						//move = console.readLine();
						move = BreakThrough.getInstance().nextMove(lastMove);
						System.out.println("TRYING :: " + move);
						Client.output.write(move.getBytes(),0,move.length());
						Client.output.flush();
						break;
		            
		            // Start as black player

	            	case '2':
		                System.out.println("New game! Your playing as Black. Wait for your opponent to move...");
		                aBuffer = new byte[1024];
						
						size = input.available();
						//System.out.println("size " + size);
						input.read(aBuffer,0,size);
		                s = new String(aBuffer).trim();
		                boardValues = s.split(" ");
		                x = 0;
		                y = 0;
		                for(int i=0; i<boardValues.length;i++){
		                    board[y][x] = Integer.parseInt(boardValues[i]);
		                    x++;
		                    if(x == 8){
		                        x = 0;
		                        y++;
		                    }
		                }
		            	BreakThrough.getInstance().setUp(board, BreakThrough.BLACK);
						break;
	
	
					// Server asking next move (last move also available)
	            	case '3':
						aBuffer = new byte[16];
						
						size = input.available();
						//System.out.println("size " + size);
						input.read(aBuffer, 0, size);
						
						lastMove = new String(aBuffer);
						System.out.println("Last move is : " + lastMove + "\n Enter your move : ");
				       	System.out.println(" ");
						move = null;
						//move = console.readLine();
						move = BreakThrough.getInstance().nextMove(lastMove);
						System.out.println("TRYING :: " + move);
						Client.output.write(move.getBytes(),0,move.length());
						Client.output.flush();
						break;
					
					// Last move is invalid
	            	case '4':
						System.out.println("That move is invalid. Enter your move : ");
						Client.disconnect();
						if(Math.random() > 0)
							break;
				       	move = null;
						//move = console.readLine();
						move = BreakThrough.getInstance().nextMove(lastMove);
						System.out.println("TRYING :: " + move);
						if(cmd == '4')
							throw new RuntimeException("END OF STREAM");
						Client.output.write(move.getBytes(),0,move.length());
						Client.output.flush();
						break;

	            	case '5':
						System.out.println("\n!!! VICTORY !!!");
						Client.disconnect();
						break;

	            	case '6':
						System.out.println("\n!!! DEFEAT !!!");
						Client.disconnect();
						break;
						
					default:
						break;
				}
	        }
		}
		catch (IOException exc) 
		{
			throw new IOException("Client connection failure", exc);
		}
		finally
		{			
			Client.disconnect();
		}
	}
	
	public static final void disconnect(){
		try 
		{
			if (Client.console != null)
				Client.console.close();
		} 
		catch (IOException exc) 
		{
			System.out.println("Error will closing console\n" + exc);
		}
		
		finally
		{ 
			Client.isConnected = false;
			try 
			{
				if (Client.output != null)
					Client.output.close();
			} 
			catch (IOException exc) 
			{
				System.out.println("Error will closing output pipe\n" + exc);
			}

			finally
			{ 
				try 
				{
					if (Client.input != null)
						Client.input.close();
				} 
				catch (IOException exc) 
				{
					System.out.println("Error will closing input pipe\n" + exc);
				}
				finally
				{ 
				
					try 
					{
						if (Client.myClient != null)
							Client.myClient.close();
					} 
					catch (IOException exc) 
					{
						System.out.println("Error will closing connection to server\n" + exc);
					}
				}// </finally input>
			}//  </finally output>
		}//  </finally console>
	}
}

/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-10              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.evals;

import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;



public final class EvalRandom implements IBoardEvaluators
{
	
	private int incr;
	
	public EvalRandom() 
	{
		super();
		this.incr = 0;
	}
	
	@Override
	public final String toString()
	{
		return "[EvalRandom: call=" + this.incr + "]";
	}

	@Override
	public int evalBoard(final PlayerWhite player, final int depth)
	{
		int score = (int)(Math.random() * 100);
		this.incr++;
		//System.out.print("(s:" + score + "), ");
		//int score = this.incr++;
		//System.out.println("-------------------------------------------");
		//System.out.println("pla=" + player);
		//System.out.println("scr=" + score);
		//System.out.println("pos=" + Long.toBinaryString(playerPositions));
		//System.out.println("enn=" + Long.toBinaryString(opponentPositions));
		return score;
	}

	@Override
	public int evalBoard(final PlayerBlack player, final int depth)
	{
		int score = (int)(Math.random() * 100);
		this.incr++;
		//System.out.print("(s:" + score + "), ");
		//int score = this.incr++;
		//System.out.println("-------------------------------------------");
		//System.out.println("pla=" + player);
		//System.out.println("scr=" + score);
		//System.out.println("pos=" + Long.toBinaryString(playerPositions));
		//System.out.println("enn=" + Long.toBinaryString(opponentPositions));
		return score;
	}
}

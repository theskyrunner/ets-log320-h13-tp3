/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-06              Interface created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.evals;

import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;

public interface IBoardEvaluators {
	public static final int SCORE_WIN  =  100000000;
	public static final int SCORE_LOSE = -100000000;
	public static final int SCORE_MINUS_1 = 1000;
	public static final int SCORE_MINUS_2 =  800;
	public static final int SCORE_MINUS_3 =  600;
	public static final int SCORE_MINUS_4 =  400;
	public static final int SCORE_MINUS_5 =  200;

	public static final int MASK_X_PATTERN =  0x50205;
	public static final int MASK_X_OFFSET =     9;
	
	public static final long COL_A = 0x8080808080808080L;
	public static final long COL_B = 0x4040404040404040L;
	public static final long COL_C = 0x2020202020202020L;
	public static final long COL_D = 0x1010101010101010L;
	public static final long COL_E = 0x0808080808080808L;
	public static final long COL_F = 0x0404040404040404L;
	public static final long COL_G = 0x0202020202020202L;
	public static final long COL_H = 0x0101010101010101L;
	public static final long ROW_1 = 0x00000000000000FFL;
	public static final long ROW_2 = 0x000000000000FF00L;
	public static final long ROW_3 = 0x0000000000FF0000L;
	public static final long ROW_4 = 0x00000000FF000000L;
	public static final long ROW_5 = 0x000000FF00000000L;
	public static final long ROW_6 = 0x0000FF0000000000L;
	public static final long ROW_7 = 0x00FF000000000000L;
	public static final long ROW_8 = 0xFF00000000000000L;
	public int evalBoard(final PlayerWhite player, final int depth);
	public int evalBoard(final PlayerBlack player, final int depth);
}

/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-04-05              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.evals;

import java.util.Arrays;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;
import ca.etsmtl.cours.log320.breakTrought.moves.AMoveGenerator;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerBlack;
import ca.etsmtl.cours.log320.breakTrought.moves.PlayerWhite;

public final class EvalSimple implements IBoardEvaluators
{	
	private static final int[] rowValues = new int[0x100];
	static {
		for (int i = 0; i < 0x100; i++)
		{
			int val = i;
			while(0 != val)
			{
				if((val & 1) != 0)
					EvalSimple.rowValues[i]++;
				val >>= 1;				
			}
		}
		
	}

	private static final int POS = 0;
	private static final int POS_PIECES = 1;
	private static final int OLD = 2;
	private static final int OLD_PIECES = 3;
	private static final int OPP = 4;
	private static final int OPP_PIECES = 5;
	private static final int ODP = 6;
	private static final int ODP_PIECES = 7;
	private static final int CLUSTER = 8;
	private static final int mask = 0xFF;
	private static final int[] scoresBlack = {0, 0, SCORE_MINUS_5, SCORE_MINUS_4, SCORE_MINUS_3, SCORE_MINUS_2, SCORE_MINUS_1, SCORE_WIN};
	private static final int[] scoresWhite = {SCORE_WIN, SCORE_MINUS_1, SCORE_MINUS_2, SCORE_MINUS_3, SCORE_MINUS_4, SCORE_MINUS_5, 0, 0};
	
	private final long[] pos;
	
	public EvalSimple() 
	{
		super();
		this.pos = new long[CLUSTER];
	}

	@Override
	public int evalBoard(PlayerWhite player, final int depth) 
	{
		if(0 != (IBoardEvaluators.ROW_8 & player.getInfo(depth)))
		{
			return IBoardEvaluators.SCORE_WIN;
		}
		if(0 != (IBoardEvaluators.ROW_1 & player.getOpponent().getInfo(depth)))
		{
			return IBoardEvaluators.SCORE_LOSE;
		}

		int score = 0;
		this.pos[POS] = player.getInfo(depth);
		this.pos[POS_PIECES] = 0;
		this.pos[OLD] = player.getInfo(depth  - AMoveGenerator.KEY_CLUSTER_SIZE);
		this.pos[OLD_PIECES] = 0;
		this.pos[OPP] = player.getOpponent().getInfo(depth);
		this.pos[OPP_PIECES] = 0;
		this.pos[ODP] = player.getOpponent().getInfo(depth - AMoveGenerator.KEY_CLUSTER_SIZE);
		this.pos[ODP_PIECES] = 0;
		int compt, a;
		for (compt = 0; compt < BreakThrough.ROW_LENGTH; compt++)
		{
			this.pos[POS_PIECES] += EvalSimple.rowValues[(int)((this.pos[POS] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[OLD_PIECES] += EvalSimple.rowValues[(int)((this.pos[OLD] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[OPP_PIECES] += EvalSimple.rowValues[(int)((this.pos[OPP] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[ODP_PIECES] += EvalSimple.rowValues[(int)((this.pos[ODP] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			a = EvalSimple.rowValues[(int)((this.pos[POS] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			score += a * EvalSimple.scoresWhite[compt];
		}
		//System.out.println(score + "|" + Arrays.toString(this.pos));
		return score;
	}

	@Override
	public int evalBoard(PlayerBlack player, final int depth) 
	{
		if(0 != (IBoardEvaluators.ROW_1 & player.getInfo(depth)))
		{
			return IBoardEvaluators.SCORE_WIN;			
		}
		if(0 != (IBoardEvaluators.ROW_8 & player.getOpponent().getInfo(depth)))
		{
			return IBoardEvaluators.SCORE_LOSE;
		}

		int score = 0;
		this.pos[POS] = player.getInfo(depth);
		this.pos[POS_PIECES] = 0;
		this.pos[OLD] = player.getInfo(depth  - AMoveGenerator.KEY_CLUSTER_SIZE);
		this.pos[OLD_PIECES] = 0;
		this.pos[OPP] = player.getOpponent().getInfo(depth);
		this.pos[OPP_PIECES] = 0;
		this.pos[ODP] = player.getOpponent().getInfo(depth - AMoveGenerator.KEY_CLUSTER_SIZE);
		this.pos[ODP_PIECES] = 0;
		int compt, a;
		for (compt = 0; compt < BreakThrough.ROW_LENGTH; compt++)
		{
			this.pos[POS_PIECES] += EvalSimple.rowValues[(int)((this.pos[POS] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[OLD_PIECES] += EvalSimple.rowValues[(int)((this.pos[OLD] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[OPP_PIECES] += EvalSimple.rowValues[(int)((this.pos[OPP] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			this.pos[ODP_PIECES] += EvalSimple.rowValues[(int)((this.pos[ODP] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			a = EvalSimple.rowValues[(int)((this.pos[POS] >> (compt *  BreakThrough.ROW_LENGTH)) & mask)];
			score += a * EvalSimple.scoresBlack[compt];
		}
		//System.out.println(score + "|" + Arrays.toString(this.pos));
		return score;
	}
}

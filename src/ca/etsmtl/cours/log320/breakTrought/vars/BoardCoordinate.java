/*******************************************************************************
 * Project :                Lab3 - BreakTrought Game
 * Course :                 LOG320
 * Session :                Winter 2013
 * Group :                  01
 * Team :                   None
 * Student :                Nicolas Lefebvre            LEFN06078909
 * Teacher :                Patrick Cardinal
 * Lab Overseer :           Francis Cardinal
 * Created :                See modification history
 * Last modified :          See modification history
 * =============================================================
 *  Modification history
 * =============================================================
 *  2013-03-08              Class created;
 *******************************************************************************/
package ca.etsmtl.cours.log320.breakTrought.vars;

import ca.etsmtl.cours.log320.breakTrought.BreakThrough;

public enum BoardCoordinate {
	A(), B(), C(), D(), E(), F(), G(), H();
	
	public static final int OFFSET_CAPITAL_LETTER = 0x40;
	
	public static final int coordinate2offset(final String coord)
	{
		int col = BreakThrough.ROW_LENGTH - (BoardCoordinate.valueOf(coord.substring(0, 1)).ordinal() + 1);
		int row = Integer.parseInt(coord.substring(1, 2)) - 1;
		return (row * BreakThrough.ROW_LENGTH) + col;
	}
	
	public static final String offset2coordinate(final int index)
	{
		char col = (char) (BoardCoordinate.OFFSET_CAPITAL_LETTER + (BreakThrough.ROW_LENGTH - (index % 8)));
		int  row = ((int) (index / 8)) + 1;
		return "" + col + row;
	}
}
